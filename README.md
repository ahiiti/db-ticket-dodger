DB Ticket Dodger
----------------
(this issue already has been reported to DB)

This application searches for Deutsche Bahn's Tickets booked via their
event ticket booking site [https://vat.db-app.de/certify?event=879].

When booking a ticket, the user is given a link in the format
`https://vat.db-app.de/getOnlineTicket/<booking number>.pdf` to download
their ticket.

The only distinction between the URLs of two tickets is their booking number.

Apparently, the booking number is always a 6-digit code consisting of
uppercase letters (A-Z) and digits (0-9).

Such a limited set of URLs makes guessing valid URLs very easy.

With a character set of 36 characters, there would be 36^6 = 2,176,782,336 tickets.
Assuming there is a reasonable number of tickets online, we should be able to
guess a valid ticket number for around every million tries.

This program generates random booking numbers and tries to download the
respective ticket file.

Usage:

    ./ticketdodger.py -h

Example:

    ./ticketdodger.py -o ~/Downloads/tickets
