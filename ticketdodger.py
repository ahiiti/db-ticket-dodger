#!/usr/bin/env python3

import requests
import argparse
import random
import os
import datetime
import signal

parser = argparse.ArgumentParser()

parser.add_argument('--number', '-n', type=int, help='Number of URLs to try', default=None)
parser.add_argument('--output_dir', '-o', help='Directory to save tickets to', default='.')
args = parser.parse_args()

# Ticket IDs seem to have a length of 6 with characters from A-Z and 0-9
id_chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'
id_length = 6

def gen_id():
    return ''.join([random.choice(id_chars) for _ in range(id_length)])

def gen_url():
    id = gen_id()
    return id, f'https://vat.db-app.de/getOnlineTicket/{id}.pdf'

# handling of termination
terminated = False
def terminate(signum, frame):
    global terminated
    terminated = True

signal.signal(signal.SIGINT, terminate)
signal.signal(signal.SIGTERM, terminate)

start_time = datetime.datetime.now()

# define and create folders for output
ticket_dir = os.path.join(args.output_dir, 'tickets')
error_dir = os.path.join(args.output_dir, 'errors')
os.makedirs(args.output_dir, exist_ok=True)
os.makedirs(ticket_dir, exist_ok=True)
os.makedirs(error_dir, exist_ok=True)

i = 0
successes = 0
errors = 0

while not terminated and (args.number is None or i < args.number):
    try:
        if i % 100 == 0:
            with open(os.path.join(args.output_dir, 'status.txt'), 'w') as status_file:
                status_file.write(f'{i} tries ({successes} successes) since {start_time.year}-{start_time.month:02}-{start_time.day:02} {start_time.hour:02}:{start_time.minute:02}')
                status_file.close()

        i += 1
        id, url = gen_url()
        print(f'[{i}] [{id}] ({successes} so far) Getting {url} ...', end='', flush=True)
        head = requests.head(url)
        length = 0 if 'Content-Length' not in head.headers else int(head.headers['Content-Length'])
        if length == 0:
            print(f' Nothing there... (Code {head.status_code})', flush=True)
        elif head.status_code == 200:
            print(f' Found something! Downloading...', end='', flush=True)
            response = requests.get(url)
            file_path = os.path.join(ticket_dir, f'{id}.pdf')
            with open(file_path, 'wb') as file:
                file.write(response.content)
                file.close()
                print(f' Saved to {file_path}', flush=True)
                successes += 1
        else:
            response = requests.get(url)
            extension = response.headers['content-type'].split('/')[-1]
            file_path = os.path.join(error_dir, f'{id}.{extension}')
    
    except:
        print(' Error!')

print('Exiting.')
